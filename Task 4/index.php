<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task 4</title>
</head>
<body>

    <?php
    
    // return a new array containing the common elements of the given two arrays.
    function compareArrays($a1, $a2)
    {
        //put your code here
        var_dump(array_intersect($a1, $a2));
    }

    compareArrays(['red', 'green', 'yellow'], ['red', 'green', 'black']);
    compareArrays(['a', 'b', 'c', 'd', 'e'], ['a', 'b', 'c', 'd', 'e']);
    compareArrays(['a'], ['a', 'b']);
    compareArrays(['a'], ['a', 'c']);
    compareArrays(['a', 'ac', 'eb'], ['a', 'ca', 'be']);
    compareArrays(['a', 'b', 'c'], ['a', 'b', 'c']);

    ?>

</body>
</html>
window.onload = init()

function init(){
    box = sel('#box')[0];
    r = sel('#r')[0];
    g = sel('#g')[0];
    b = sel('#b')[0];
    color = {
        r: 180,
        g: 180,
        b: 180
    }

    changeColor(color)

    listeners();
}

function sel(el){
    return document.querySelectorAll(el);
}

function changeColor({r, g, b}){
    box.style.backgroundColor = `rgb(${r}, ${g} , ${b})`
    box.innerText = `rgb(${r}, ${g}, ${b})`
}

function listeners(){
    r.addEventListener('change', function(){
        color.r = this.value
        changeColor(color)
    })

    g.addEventListener('change', function(){
        color.g = this.value
        changeColor(color)
    })

    b.addEventListener('change', function(){
        color.b = this.value
        changeColor(color)
    })
}